package com.example.pp_tith_ouddom_jpa_hibernate01_homework.repository;

import com.example.pp_tith_ouddom_jpa_hibernate01_homework.entity.Employee;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
@Data
@AllArgsConstructor
public class EmployeeRepository {

    @PersistenceContext
    private EntityManager em;
    @Transactional
    public Employee insert(Employee employee){
        em.persist(employee);
        return employee;
    }
    @Transactional
    public Employee detach(Employee employee){
        em.detach(employee);
        Employee employee1 = new Employee();
        employee1.setBithDate(employee.getBithDate());
        employee1.setFirstName(employee.getFirstName());
        employee1.setLastName(employee.getLastName());
        employee1.setEmail(employee.getEmail());
        employee1.setTemp(employee.getTemp());
        em.merge(employee1);
        em.flush();
        return employee;
    }

    @Transactional
    public Employee findById(Integer id){
       return em.find(Employee.class,id);
    }

    @Transactional
    public void remove(Integer id){
        Employee employee = findById(id);
        em.remove(employee);
    }

}
