package com.example.pp_tith_ouddom_jpa_hibernate01_homework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpTithOuddomJpaHibernate01HomeworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(PpTithOuddomJpaHibernate01HomeworkApplication.class, args);
    }

}
