package com.example.pp_tith_ouddom_jpa_hibernate01_homework.controller;

import com.example.pp_tith_ouddom_jpa_hibernate01_homework.entity.Employee;
import com.example.pp_tith_ouddom_jpa_hibernate01_homework.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {
    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @PostMapping("/persist")
    Employee insert(){
        return employeeRepository.insert(new Employee(null,"Tith","Ouddom","Ouddom@gmail.com",new Date(),"PP"));
    }

    @PostMapping ("/detach")
    Employee detach(){
        return employeeRepository.detach(new Employee(null,"Tith","Ouddom","tithouddom@gmail.com",new Date(),"PP"));
    }

    @GetMapping("/find/{id}")
    Employee findById(@PathVariable Integer id){
        return employeeRepository.findById(id);
    }

    @DeleteMapping("/remove/{id}")
    void remove(@PathVariable Integer id){
        employeeRepository.remove(id);
    }

}
